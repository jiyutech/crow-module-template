import intro from './index';

test('module export should be an object', () => {
  expect(typeof intro).toBe('object');
});
