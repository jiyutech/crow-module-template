// module.exports = require('./lib/index')

export class DelayRunner {

  run( callback, timeout ){
    clearTimeout( this.timer );
    this.timer = setTimeout( callback || function(){}, timeout || 0);
  }

  clear(){
    clearTimeout( this.timer );
  }

}
