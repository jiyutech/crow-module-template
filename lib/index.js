"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// module.exports = require('./lib/index')

var DelayRunner = exports.DelayRunner = function () {
  function DelayRunner() {
    _classCallCheck(this, DelayRunner);
  }

  _createClass(DelayRunner, [{
    key: "run",
    value: function run(callback, timeout) {
      clearTimeout(this.timer);
      this.timer = setTimeout(callback || function () {}, timeout || 0);
    }
  }, {
    key: "clear",
    value: function clear() {
      clearTimeout(this.timer);
    }
  }]);

  return DelayRunner;
}();